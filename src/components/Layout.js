import {
  Drawer,
  ListItem,
  List,
  Typography,
  withStyles
} from "@material-ui/core/";
import PropTypes from "prop-types";
import React, { Component } from "react";
import { Link, Route, withRouter } from "react-router-dom";

import EmployeesPage from "../containers/EmployeesPage";
import StudentsPage from "../containers/StudentsPage";
import UsersPage from "../containers/UsersPage";

class Layout extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Drawer
          variant="permanent"
          classes={{
            paper: classes.drawerPaper
          }}
        >
          <div className={classes.toolbar} />
          <List component="nav">
            <Link
              to="/users"
              className ={classes.link}
            >
              <ListItem button>Users</ListItem>
            </Link>
            <Link
              to="/students"
              className ={classes.link}
            >
              <ListItem button>Students</ListItem>
            </Link>
            <Link
              to="/employees"
              className ={classes.link}
            >
              <ListItem button>Employees</ListItem>
            </Link>
          </List>
        </Drawer>
        <main>
          <div className={classes.body}>
            {
              <Typography>
                <Route path="/users" component={UsersPage} />
                <Route path="/employees" component={EmployeesPage} />
                <Route path="/students" component={StudentsPage} />
              </Typography>
            }
          </div>
        </main>
      </div>
    );
  }
}

const styles = theme => ({
  root: {
    height: 630,
    overflow: "hidden",
    position: "relative",
    display: "flex"
  },
  body: {
    height: 580,
    overflow: "auto",
    position: "relative",
    display: "flex"
  },
  drawerPaper: {
    position: "relative",
    width: 200
  },
  link: {
    textDecoration:"none",
    color: "black"
  },
  toolbar: theme.mixins.toolbar
});

Layout.propTypes = {
  classes: PropTypes.object.isRequired
};

const styledComponent = withStyles(styles)(Layout);
export default withRouter(styledComponent);
