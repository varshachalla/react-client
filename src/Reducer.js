import { FETCH_DATA_SUCCESS, FETCH_DATA_FAILURE } from "./Actions/actions";

const initialState = {
  items: [],
  error: null
};

export default function Reducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_DATA_SUCCESS:
      return {
        ...state,
        items: action.payload
      };

    case FETCH_DATA_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        items: []
      };

    default:
      return state;
  }
}
