import { AppBar, Toolbar, Typography, withStyles } from "@material-ui/core";
import React, { Component, Fragment } from "react";
import { connect } from "react-redux";

import { fetchData } from "../Actions/actions";
import Table from "../components/Table";

const columns = [
  { label: "name", key: "name" },
  { label: "username", key: "username" },
  { label: "email", key: "email" },
  { label: "phone", key: "phone" }
];

const styles = theme => ({
  grow: {
    flexGrow: 1
  },
  title: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block"
    }
  }
});

class UsersPage extends Component {
  render() {
    const { classes, data } = this.props;
    return (
      <Fragment>
        <AppBar position="static">
          <Toolbar>
            <Typography
              className={classes.title}
              variant="h6"
              color="inherit"
              noWrap
            >
              Users
            </Typography>
            <div className={classes.grow} />
          </Toolbar>
        </AppBar>
        <Table data={data} columns={columns} />
      </Fragment>
    );
  }

  componentDidMount() {
    this.props.dispatch(fetchData("users"));
  }
}

const mapStateToProps = state => {
  return {
    data: state.items.data
  };
};

const styledComponent = withStyles(styles)(UsersPage);
export default connect(mapStateToProps)(styledComponent);
